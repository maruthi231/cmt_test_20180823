export var depositMethods = {
  "CREDITCARD":{
    "txType": "CreditcardDeposit",
    "displayName": "Visa/Mastercard",
    "providerName": "CREDITCARD",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-8'
      },
      {
        "id": "cardHolder",
        "name": "Card Holder Name",
        "key": "cardHolder",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "creditcardNumber",
        "name": "Creditcard Number",
        "key": "CreditcardNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "cvv",
        "name": "CVV",
        "key": "cvv",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-3 centre-text'
      },
      {
        "id": "recvv",
        "name": "CVV",
        "key": "cvv",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-3 centre-text pull-right recvv'
      },
      {
        "id": "expiryLabel",
        "name": "Expiry",
        "key": "expiryLabel",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"label",
        "class":'col-xs-3 expiry-label'
      },
      {
        "id": "expiryMonth",
        "name": "MM",
        "key": "expiryMonth",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3 centre-text'
      },
      {
        "id": "expiryYear",
        "name": "YYYY",
        "key": "expiryYear",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3  centre-text'
      },
      {
        "id": "addNewCard",
        "name": "Add New Card",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved cards",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
  "ECOPAYZ":{
    "txType": "EcoPayzDeposit",
    "displayName": "EcoPayz",
    "providerName": "ECOPAYZ",
  },
  "CASHLIB":{
    "txType": "CashlibDeposit",
    "displayName": "Cashlib",
    "providerName": "CASHLIB",
  },
  "TRUSTLY":{
    "txType": "TrustlyDeposit",
    "displayName": "Trustly",
    "providerName": "BANK",
    "serviceName":"TRUSTLY",
    "inputs": []
  },
  "BANK":{
    "txType": "BankDeposit",
    "displayName": "Bank Deposit",
    "providerName": "BANK",
    "serviceName":"BANK",
    "inputs": []
  },
  "CITADEL":{
    "txType": "CitadelDeposit",
    "displayName": "Citadel",
    "providerName": "BANK",
    "serviceName":"CITADEL",
    "inputs": []
  },
  "INTERAC":{
    "txType": "BankDeposit",
    "displayName": "Interac Direct Deposit",
    "providerName": "BANK",
    "serviceName":"INTERAC",
    "inputs": []
  },
  "FLEXEPIN":{
    "txType": "FlexepinDeposit",
    "displayName": "Flexepin",
    "providerName": "FLEXEPIN",
    "inputs": [
      {
        "id": "voucherNumber",
        "name": "Voucher Number",
        "key": "voucherNumber",
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      }
    ]
  },
  "NEOSURFVOUCHER":{
    "txType": "neosurfVoucher",
    "displayName": "Neosurf Voucher",
    "providerName": "NEOSURFVOUCHER",
    "inputs": [
      {
        "id": "voucherNumber",
        "name": "Voucher Number",
        "key": "voucherNumber",
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      }
    ]
  },
  "INSTADEBIT":{
    "txType": "InstadebitDeposit",
    "displayName": "Instadebit",
    "providerName": "INSTADEBIT",
  },
  "UPAYCARD":{
    "txType": "UpaycardDeposit",
    "displayName": "Upaycard",
    "providerName": "UPAYCARD",
    "inputs": [
      {
        "id": "accountNumber",
        "name": "Account Number",
        "key": "accountNumber",
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      }
    ]
  },
  "NETELLER":{
    "txType": "NetellerDeposit",
    "displayName": "Neteller",
    "providerName": "NETELLER",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "account",
        "name": "Neteller Account",
        "key": "account",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "secureId",
        "name": "secureId",
        "key": "secureId",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "addNewCard",
        "name": "New Account",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ]
  },
  "QIWI":{
    "txType": "QiwiDeposit",
    "displayName": "Qiwi",
    "providerName": "QIWI",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "phoneNumber",
        "name": "Phone Number",
        "key": "phoneNumber",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "addNewCard",
        "name": "New Account",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ]
  },
  "NEOSURF":{
    "txType": "NeosurfDeposit",
    "displayName": "Neosurf",
    "providerName": "CREDITCARD",
    "serviceName":"NEOSURF",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-8'
      },
      {
        "id": "cardHolder",
        "name": "Card Holder Name",
        "key": "cardHolder",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "creditcardNumber",
        "name": "Creditcard Number",
        "key": "CreditcardNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "cvv",
        "name": "CVV",
        "key": "cvv",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-3 centre-text'
      },
      {
        "id": "recvv",
        "name": "CVV",
        "key": "cvv",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-3 centre-text pull-right recvv'
      },
      {
        "id": "expiryLabel",
        "name": "Expiry",
        "key": "expiryLabel",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"label",
        "class":'col-xs-3 expiry-label'
      },
      {
        "id": "expiryMonth",
        "name": "MM",
        "key": "expiryMonth",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3 centre-text'
      },
      {
        "id": "expiryYear",
        "name": "YYYY",
        "key": "expiryYear",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3  centre-text'
      },
      {
        "id": "addNewCard",
        "name": "Add New Card",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved cards",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
  "SKRILL":{
    "txType": "SkrillDeposit",
    "displayName": "Skrill",
    "providerName": "SKRILL",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "email",
        "name": "Email",
        "key": "email",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "addNewCard",
        "name": "New Account",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ]
  },
  "IDEBIT":{
    "txType": "IDebitDeposit",
    "displayName": "IDebit",
    "providerName": "IDEBIT",
  },
  "PAYSAFECARD":{
    "txType": "PaysafeCard",
    "displayName": "Paysafe Card",
    "providerName": "PAYSAFECARD",
  },
}
