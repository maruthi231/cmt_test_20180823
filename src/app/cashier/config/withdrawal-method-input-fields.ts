export var withdrawalMethods = {
  "CREDITCARD":{
    "txType": "CreditcardWithdrawal",
    "displayName": "Visa/Mastercard",
    "providerName": "CREDITCARD",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "cardHolder",
        "name": "Card Holder Name",
        "key": "cardHolder",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "creditcardNumber",
        "name": "Creditcard Number",
        "key": "CreditcardNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "isEncrypted":true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "expiryLabel",
        "name": "Expiry",
        "key": "expiryLabel",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"label",
        "class":'col-xs-3 expiry-label'
      },
      {
        "id": "expiryMonth",
        "name": "MM",
        "key": "expiryMonth",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3 centre-text'
      },
      {
        "id": "expiryYear",
        "name": "YYYY",
        "key": "expiryYear",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"tel",
        "class":'col-xs-3  centre-text'
      },
      {
        "id": "addNewCard",
        "name": "Add New Card",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved cards",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
  "SKRILL":{
    "txType": "SkrillWithdrawal",
    "displayName": "Skrill",
    "providerName": "SKRILL",
    "inputs": [
      {
        "id": "skrillEmail",
        "name": "Email",
        "key": "skrillEmail",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"email",
        "class":'col-xs-12'
      }
    ],
  },
  "NETELLER":{
    "txType": "NetellerWithdrawal",
    "displayName": "Neteller",
    "providerName": "NETELLER",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "account",
        "name": "Neteller Account",
        "key": "account",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "addNewCard",
        "name": "New Account",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ]
  },
  "ECOPAYZ":{
    "txType": "EcoPayzWithdrawal",
    "displayName": "EcoPayz",
    "providerName": "ECOPAYZ",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
  "INSTADEBIT":{
    "txType": "InstadebitWithdrawal",
    "displayName": "Instadebit",
    "providerName": "INSTADEBIT",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
  "UPAYCARD":{
    "txType": "UpaycardWithdrawal",
    "displayName": "Upaycard",
    "providerName": "UPAYCARD",
    "inputs": [
      {
        "id": "accountNumber",
        "name": "Account Number",
        "key": "accountNumber",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "accountNumber",
        "name": "Account Number",
        "key": "accountNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"text",
        "class":'col-xs-12'
      },
    ],
  },
  "QIWI":{
    "txType": "QiwiWithdrawal",
    "displayName": "Qiwi",
    "providerName": "QIWI",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "phoneNumber",
        "name": "Phone Number",
        "key": "phoneNumber",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "type":"tel",
        "class":'col-xs-12'
      },
      {
        "id": "addNewCard",
        "name": "New Account",
        "key": "addNewCard",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"button",
        "class":'change-cc'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ]
  },
  "BANKLOCAL":{
    "txType": "BankLocalWithdrawal",
    "displayName": "Bank Local Withdrawal",
    "providerName": "BANKLOCAL",
    "inputs": [
      {
        "id": "clearingNumber",
        "name": "Clearing Number",
        "key": "clearingNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "accountNumber",
        "name": "Account Number",
        "key": "accountNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "beneficiaryName",
        "name": "Beneficiary Number",
        "key": "beneficiaryName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "bankName",
        "name": "Bank Name",
        "key": "bankName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
    ],
  },
  "BANKIBAN":{
    "txType": "BankIBANWithdrawal",
    "displayName": "Bank IBAN Withdrawal",
    "providerName": "BANKIBAN",
    "inputs": [
      {
        "id": "bic",
        "name": "BIC/SWIFT",
        "key": "bic",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "iban",
        "name": "IBAN",
        "key": "iban",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "beneficiaryName",
        "name": "Beneficiary Number",
        "key": "beneficiaryName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
      {
        "id": "bankName",
        "name": "Bank Name",
        "key": "bankName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-12'
      },
    ],
  },
  "BANKINTL":{
    "txType": "BankIntlWithdrawal",
    "displayName": "Bank International (SWIFT) Withdrawal",
    "providerName": "BANKINTL",
    "inputs": [
      {
        "id": "bic",
        "name": "BIC/SWIFT code",
        "key": "bic",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "accountNumber",
        "name": "Account Number",
        "key": "accountNumber",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryName",
        "name": "Beneficiary Number",
        "key": "beneficiaryName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryStreet",
        "name": "Beneficiary Street",
        "key": "beneficiaryStreet",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryZip",
        "name": "Beneficiary Zip",
        "key": "beneficiaryZip",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryCity",
        "name": "Beneficiary City",
        "key": "beneficiaryCity",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryState",
        "name": "Beneficiary State",
        "key": "beneficiaryState",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "beneficiaryCountry",
        "name": "Beneficiary Country",
        "key": "beneficiaryCountry",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "branchAddress",
        "name": "Branch Address",
        "key": "branchAddress",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "branchCode",
        "name": "Branch Code",
        "key": "branchCode",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
      {
        "id": "bankName",
        "name": "Bank Name",
        "key": "bankName",
        "requiredNewPayment": true,
        "visibleOnNewPayment": true,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"text",
        "class":'col-xs-6'
      },
    ],
  },
  "IDEBIT":{
    "txType": "IDebitWithdrawal",
    "displayName": "IDebit",
    "providerName": "IDEBIT",
    "inputs": [
      {
        "id": "accountId",
        "name": "Account Id",
        "key": "accountId",
        "requiredNewPayment": false,
        "visibleOnNewPayment": false,
        "requiredRepeatPayment": true,
        "visibleOnRepeatPayment": true,
        "type":"radio",
        "class":'col-xs-12'
      },
      {
        "id": "backToSavedCards",
        "name": "Back to saved accounts",
        "key": "backToSavedCards",
        "requiredNewPayment": "basedOnAccounts",
        "visibleOnNewPayment": "basedOnAccounts",
        "requiredRepeatPayment": false,
        "visibleOnRepeatPayment": false,
        "type":"button",
        "class":'change-cc'
      }
    ],
  },
}
