import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { AjaxService } from '../../services/ajax.service';
import { CustomValidators } from '../../utility/custom-validator';
import { environment } from '../../../environments/environment';
import { CashierService } from '../services/cashier.service';
import { UserDetailsService } from '../../services/user-details.service';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';
import { depositMethods } from '../config/deposit-method-input-fields';

import * as _ from 'underscore';
declare var $:any;
@Component({
  selector: 'app-quick-deposit-footer',
  templateUrl: './quick-deposit-footer.component.html',
  styleUrls: ['./quick-deposit-footer.component.scss']
})
export class QuickDepositFooterComponent extends FormValidationComponent  implements OnInit {

  userProfileSubscription;
  savedCards;
  selectedCard;
  hideDeposit;
  constructor(
    private utils:Utility,
    private ajaxService:AjaxService,
    private fb: FormBuilder,
    private CustomValidators:CustomValidators,
    private cashierService:CashierService,
    private userDetailsService:UserDetailsService
  ) {
    super(utils);
    this.userProfileSubscription = this.userDetailsService.userProfileUpdated$.subscribe(
      profileUpdateMessage => {
        if(profileUpdateMessage == "UserProfileUpdated"){
          this.getSavedCards();
        }
      }
    );
  }

  callBackUrls;
  cashierDetailsForm = this.fb.group({
    'amount': ['', [this.CustomValidators.required]],
    'merchantId': [ environment.paymentIqMID , [this.CustomValidators.required]],
    'userId': [environment.paymentIqUID, [this.CustomValidators.required]],
    'sessionId': ['1234',this.CustomValidators.required]
  });

  cashierAttributesForm = this.fb.group({
    'successUrl': [''],
    'failureUrl': [''],
    'pendingUrl': [''],
    'labelId': [''],
    'productId': [''],
    'ipAddr': [''],
    'bnsCode': ['']
  });

  creditCardForm = this.fb.group({
    'accountId': [''],
    'cvv': ['']
  });

  ngOnInit() {
    this.hideDeposit = true;
    this.getToken();
  }

  openOrDeposit(){
    if($(".quick-deposit-f-holder").hasClass("hide")){
      this.savedCards = undefined;
      $(".quick-deposit-f-holder").removeClass("hide");
      this.creditCardForm.controls["cvv"].setValue("");
      this.cashierDetailsForm.controls["amount"].setValue("");
      this.creditCardForm.controls["cvv"].markAsUntouched();
      this.cashierDetailsForm.controls["amount"].markAsUntouched();
      this.creditCardForm.controls["cvv"].markAsPristine()
      this.cashierDetailsForm.controls["amount"].markAsPristine();
      this.hideAndShowButtons("TopUp");
      this.getToken();
    }else{
      this.creditCardForm.controls["accountId"].setValue(this.savedCards[this.selectedCard].accountId)
      if(this.creditCardForm.valid && this.cashierDetailsForm.valid){
        this.utils.disableButton("topUpButton","",true);
        Promise.resolve(this.cashierService.makeTransaction(this.creditCardForm,this.cashierDetailsForm,depositMethods["CREDITCARD"],'deposit',this.cashierAttributesForm))
        .then(
          depositData => {
            this.utils.enableButton("topUpButton","");
            $(".quick-deposit-f-holder").addClass('hide');
            $(".quick-deposit-f-msg").removeClass('hide');
            if(depositData && depositData["status"] == "success" && depositData["response"] && depositData["response"]["txState"] == "SUCCESSFUL"){
              $(".quick-deposit-f-msg .icon").removeClass('failure').addClass('success');
              $(".quick-deposit-f-msg .icon i").removeClass('icon-close').addClass('icon-tick');
              $(".quick-deposit-f-msg .text h5").html("Top up success");
              $(".quick-deposit-f-msg .text p").html("For transaction details check your history");
              this.hideAndShowButtons("ok");
            }else{
              $(".quick-deposit-f-msg .icon").addClass('failure').removeClass('success');
              $(".quick-deposit-f-msg .icon i").addClass('icon-close').removeClass('icon-tick');
              $(".quick-deposit-f-msg .text h5").html("Top up Unsuccessful");
              $(".quick-deposit-f-msg .text p").html("This transaction could not be processed");
              this.hideAndShowButtons("retry");
            }
          }
        )
      }else{
        this.utils.validateAllFormFields(this.cashierDetailsForm);
        this.utils.validateAllFormFields(this.creditCardForm);
      }
    }
  }

  getToken(){
    Promise.resolve(this.cashierService.getCashierAccessToken())
    .then(
      tokenResponse => {
        if(tokenResponse && tokenResponse["status"] &&  tokenResponse["status"] == "SUCCESS" && tokenResponse["token"]){
          this.cashierDetailsForm.controls["sessionId"].setValue(tokenResponse["token"])
          this.cashierAttributesForm.controls["successUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
          this.cashierAttributesForm.controls["failureUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
          this.cashierAttributesForm.controls["pendingUrl"].setValue(tokenResponse["deposit_succes_callback_url"]);
          this.cashierAttributesForm.controls["labelId"].setValue(tokenResponse["labelId"]);
          this.cashierAttributesForm.controls["productId"].setValue(tokenResponse["productId"]);
          this.cashierAttributesForm.controls["ipAddr"].setValue(tokenResponse["ipAddr"]);
          this.getSavedCards();
        }

      }
    );
  }

  getSavedCards(){
    if(this.userDetailsService.getUserProfileDetails()){
    this.cashierDetailsForm.controls["userId"].setValue(this.userDetailsService.getUserProfileDetails().playerID)
    let savedCardrequestData = {
      sessionId: this.cashierDetailsForm.controls["sessionId"].value,
      type:'creditcard'
    }
    if(this.cashierDetailsForm.controls["sessionId"].value != "1234"){
      Promise.resolve(this.cashierService.getSavedCards(savedCardrequestData,this.cashierDetailsForm.controls["userId"].value))
      .then(
        savedCardResposnse =>{
          if(savedCardResposnse && savedCardResposnse["accounts"] && savedCardResposnse["accounts"].length > 0){
            this.getFormControlValidators(this.creditCardForm);
            this.cashierService.settFormGroupValidation("CREDITCARD",true,this.creditCardForm)
            this.savedCards = savedCardResposnse["accounts"];
            this.selectedCard = 0;
            this.hideDeposit = false;
            this.cashierDetailsForm.controls["amount"].setValidators(this.CustomValidators.minValueNumber(0,null,"Amount"));
          }else{
            this.savedCards = [];
            this.hideDeposit = true;
          }
          setTimeout(function(){
            $('.carousel').carousel({
              interval: false
            });
          })
        },
        SystemError =>{
          this.hideDeposit = true;
          this.savedCards = [];
        }
      )
    }
  }else{
    Promise.resolve(this.ajaxService.getProfileData())
    .then(
      profileData=>{
        this.userDetailsService.setUserCashBalance(profileData["balanceDetails"]["cash"]);
        this.userDetailsService.setUserBalanceDetails(profileData["balanceDetails"]);
        this.userDetailsService.setUserBonusBalance(profileData["balanceDetails"]["bonus"]);
        this.userDetailsService.setUserTotalBalance(profileData["balanceDetails"]["cash"] + profileData["balanceDetails"]["bonus"]);
        this.userDetailsService.setUserProfileDetails(profileData);
      }
    )
  }
  }

  getFormControlValidators(formGroup){
    var self = this;
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        formGroup.get(field).setValidators(self.cashierService.getFormControlValidators(field));
      } else if (control instanceof FormGroup) {
        this.getFormControlValidators(control);
      }
    });
  }

  moveTo(place,e){
    if(place == 'prev'){
      if(this.selectedCard > 0 ){
        this.selectedCard -= 1;
      }else{
        this.selectedCard = this.savedCards.length -1;
      }
    }else if(place == 'next'){
      if(this.selectedCard < this.savedCards.length -1 ){
        this.selectedCard += 1;
      }else{
        this.selectedCard = 0;
      }
    }
    $('.carousel').carousel(place);
    e.preventDefault();
  }

  hideAndShowButtons(buttonName){
    this.utils.hideAndShowQuickDeposit(buttonName);
  }

  closeQuickDeposit(){
    $(".quick-deposit-f-holder").addClass("hide");
    $(".quick-deposit-f-msg").addClass('hide');
    this.hideAndShowButtons("deposit");
  }

  retryQuckDeposit(){
    $(".quick-deposit-f-msg").addClass('hide');
    this.openOrDeposit();
    this.hideAndShowButtons("TopUp");
  }

}
