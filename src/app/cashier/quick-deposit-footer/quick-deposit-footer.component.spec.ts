import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickDepositFooterComponent } from './quick-deposit-footer.component';

describe('QuickDepositFooterComponent', () => {
  let component: QuickDepositFooterComponent;
  let fixture: ComponentFixture<QuickDepositFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickDepositFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickDepositFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
