import { Injectable } from '@angular/core';
import { Utility } from '../../utility/utility';
import { AjaxService } from '../../services/ajax.service';
import * as $ from 'jquery';
import * as _ from 'underscore';

@Injectable()
export class DepositService {

  constructor(
    private utils:Utility,
    private ajaxService:AjaxService
  ) { }



  getIframe(redirectOutput,selectedMethodName){
    let $content;
    let $form;
    if(!redirectOutput["html"] && _.isEmpty(redirectOutput["parameters"])){
      if(redirectOutput["container"] != "window"){
        $content = $('<'+redirectOutput["container"]+' name="cciFrame" id="loaderIframe" src="'+ redirectOutput["url"]+'" frameborder="0"  style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></'+redirectOutput["container"]+'>');
      }else{
        window.location.href = redirectOutput["url"];
      }
    }else{
      $content = $('<'+redirectOutput["container"]+' name="cciFrame" id="loaderIframe" src="" frameborder="0"  style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></'+redirectOutput["container"]+'>');
        if(redirectOutput["html"]){
          // let html = redirectOutput["html"].replace("$('#keyCode').val()","$('#loaderIframe').contents().find('#keyCode').val()");
          // $form   = $(html);
          //$($content).append($form);
          $content = redirectOutput["html"];
        }else{
          $form   = ('<form id="proxy_redirect" method="'+redirectOutput["method"]+'" action="' + redirectOutput["url"] + '" class="hide" target="_self">');
          _.forEach(redirectOutput["parameters"], function(value, key) {
            if (value != null){
                $form = $form + ('<input type="hidden" name="' + key + '" value="' + value + '" />');
            }
          });
          $form = $form + ('</form>')
          if(!redirectOutput["container"].includes("window")){
            $form = $($form);
          }

        }
    }

    return {content:$content,form:$form};

  }

}
