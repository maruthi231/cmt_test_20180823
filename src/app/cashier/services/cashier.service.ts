import { Injectable } from '@angular/core';
import { CustomValidators } from '../../utility/custom-validator';
import { AjaxService } from '../../services/ajax.service';
import { EmitterService } from '../../services/emitter.service';
import { depositMethods } from '../config/deposit-method-input-fields';
import { withdrawalMethods } from '../config/withdrawal-method-input-fields';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { environment } from '../../../environments/environment';
declare var encryptData:any;

import * as _ from 'underscore';

@Injectable()
export class CashierService {
  cashierRequestQueue = [];
  constructor(
    private CustomValidators:CustomValidators,
    private ajaxService:AjaxService,
    private emitterService:EmitterService,
    private fb:FormBuilder,
    private utils:Utility
  ) { }

  getCashierAccessToken(){
    return Promise.resolve(this.ajaxService.getCashierAccessToken())
    .then(
      tokenResponse => {
        return tokenResponse;
      }
    );
  }

  getPaymentMethod(txnType,sessionId,userId){
    // Promise.resolve(this.ajaxService.getUserPaymentAccount({"sessionId":"1234"}))
    // .then(
    //   cashierData => {
    //     console.log(cashierData);
    //   }
    // );
    let response = {}

    this.cashierRequestQueue.push(Promise.resolve(this.ajaxService.getUserAvailablePaymentMethod({"sessionId":sessionId,"method":txnType},userId)));
    if(txnType == "deposit"){
      this.cashierRequestQueue.push(this.getPendingTransactions(sessionId,userId));
    }

    return Promise.all(this.cashierRequestQueue).then(
      avMethdAndPendingTxn => {

        let avPaymentMethods = avMethdAndPendingTxn[0];
        let pendingWithdrawals;
        response["pendingWithdrawals"] = this.setPendingTransaction(txnType,avMethdAndPendingTxn[1])


        if(avPaymentMethods && avPaymentMethods["success"]){
          response["availableMethods"] = avPaymentMethods["methods"];
          response["indexedAvailableMethods"] = _.indexBy(avPaymentMethods["methods"],'providerType');
          //this.availableMethods = avPaymentMethods["methods"];
          //this.indexedAvailableMethods = _.indexBy(avPaymentMethods["methods"],'providerType');
          let formFieldResponse = this.getFormFieldsForMethod(response["availableMethods"][0].service ? response["availableMethods"][0].service : response["availableMethods"][0].providerType,response["availableMethods"][0],"any",txnType);
          let feeLimitResponse = this.setFeeAndLimit(response["availableMethods"][0]);
          response = Object.assign(response,formFieldResponse,feeLimitResponse);
          this.cashierRequestQueue = [];
          return response
        }
      }
    )

    // return Promise.resolve(this.ajaxService.getUserAvailablePaymentMethod({"sessionId":"1234","method":txnType}))
    // .then(
    //   avPaymentMethods => {
    //     if(avPaymentMethods && avPaymentMethods["success"]){
    //       response["availableMethods"] = avPaymentMethods["methods"];
    //       response["indexedAvailableMethods"] = _.indexBy(avPaymentMethods["methods"],'providerType');
    //       //this.availableMethods = avPaymentMethods["methods"];
    //       //this.indexedAvailableMethods = _.indexBy(avPaymentMethods["methods"],'providerType');
    //       let formFieldResponse = this.getFormFieldsForMethod(response["availableMethods"][0].providerType,response["availableMethods"][0],"any",txnType);
    //       let feeLimitResponse = this.setFeeAndLimit(response["availableMethods"][0]);
    //       response = Object.assign(response,formFieldResponse,feeLimitResponse);
    //       return response
    //     }
    //   }
    // );


  }

  getPendingTransactions(sessionId,userId){
    return Promise.resolve(this.ajaxService.getUserPendingTransaction({"sessionId":sessionId,"method":"Withdrawal","states":"WAITING_APPROVAL"},userId));
  }

  setPendingTransaction(txnType,pendingTxns){
    let response = []
    if(txnType == "deposit" && pendingTxns && pendingTxns["success"] && pendingTxns["transactions"].length > 0){
      response = pendingTxns["transactions"];
    }
    return response;
  }

  getFormFieldsForMethod(methodName,method,message,txnType){
    let formfieldsResponse = {}
    if((txnType == 'deposit' && depositMethods[methodName]) || (txnType == 'withdrawal' && withdrawalMethods[methodName])){
      formfieldsResponse["selectedMethodCashierData"] = method;
      formfieldsResponse["selectedMethodName"] = methodName;

      let creditCardFormFields = this.getCreditCardFormFields(message,formfieldsResponse["selectedMethodCashierData"],formfieldsResponse["selectedMethodName"],txnType);
      formfieldsResponse["usedAccount"] = creditCardFormFields["usedAccount"];
      formfieldsResponse["form"] = this.createGroup(creditCardFormFields["selectedInputs"]);
      //this.form = this.createGroup(selectedInputs);
      this.settFormGroupValidation(methodName,formfieldsResponse["usedAccount"],formfieldsResponse["form"]);
      formfieldsResponse["selectedMethod"] = txnType == "deposit" ? depositMethods[methodName] : withdrawalMethods[methodName];
      formfieldsResponse["selectedMethodInputs"] = creditCardFormFields["selectedInputs"];

      if(formfieldsResponse["usedAccount"] && formfieldsResponse["form"].controls["accountId"]){
        formfieldsResponse["form"].controls["accountId"].setValue(formfieldsResponse["selectedMethodCashierData"].accounts[0].accountId);
      }
      if(txnType == 'deposit'){
        this.emitterService.broadcastEligibleBonus("loading");
        Promise.resolve(this.ajaxService.getEligibleBonuses({"criteriaType" : "DEPOSIT","depositOption" : methodName+"_HOSTED"}))
        .then(
          eligibleBonusResponse =>{
            this.emitterService.broadcastEligibleBonus(eligibleBonusResponse["eligibleBonusList"]);
          }
        )
      }

    }else{
      console.log("No Method Found");
    }
    return formfieldsResponse;
  }

  setFeeAndLimit(method){
    let feeLimitResponse = {};
    if(method.fees.length > 0){
      feeLimitResponse["fee"] = method.fees[0].percentageFee ? method.fees[0].percentageFee+'%' :
                      method.fees[0].fixedFee ? '$'+method.fees[0].fixedFee : ''
    }
    if(method.limit){
      feeLimitResponse["minAmount"] = '$'+method.limit.min;
      feeLimitResponse["maxAmount"] = '$'+method.limit.max;
    }
    return feeLimitResponse;
  }

  getCreditCardFormFields(accountType,selectedMethodCashierData,selectedMethodName,txnType){
    let credticCardFormFields = {}
    let selectedInputs = []
    let methods = txnType == "deposit" ? depositMethods[selectedMethodName] : withdrawalMethods[selectedMethodName];
    if(selectedMethodCashierData.accounts.length > 0 && (accountType == "backToSavedCards" || accountType == "any")){
      _.each(methods.inputs,function(input){
        input.requiredRepeatPayment == true ? selectedInputs.push(input) : '';
      })
      credticCardFormFields["usedAccount"] = true;
    }else{
      _.each(methods.inputs,function(input){
        if(input.requiredNewPayment == true){
          selectedInputs.push(input)
        }else if(input.requiredNewPayment == "basedOnAccounts" && selectedMethodCashierData.accounts.length > 0){
          selectedInputs.push(input)
        }
      })
      credticCardFormFields["usedAccount"] = false;
    }
    credticCardFormFields["selectedInputs"] = selectedInputs;
    return credticCardFormFields;
  }

  createGroup(fields) {
    const group = this.fb.group({});
    fields.forEach(control => {
      let formControl = new FormControl('',this.getFormControlValidators(control.key));
      group.addControl(control.key, formControl);
    });

    return group;
  }

  getFormControlValidators(fieldName){
    let validator;
    switch(fieldName){
      case "CreditcardNumber":
      case "expiryMonth":
      case "expiryYear":
      case "accountId":
      case "account":
      case "secureId":
      case "accountNumber":
      case "countryCode":
      case "voucherNumber":
      case "clearingNumber":
      case "beneficiaryName":
      case "bankName":
      case "bic":
      case "iban":
      case "beneficiaryStreet":
      case "beneficiaryZip":
      case "beneficiaryCity":
      case "beneficiaryState":
      case "beneficiaryCountry":
      case "branchAddress":
      case "branchCode":
        validator = this.CustomValidators.required;
      break;
      case "cvv":
        validator = this.CustomValidators.reqMinMaxNum(3,4)
      break;
      case "cardHolder":
        validator = this.CustomValidators.validName(2,50);
      break;
      case "email":
        validator = this.CustomValidators.validateUniqueness("txtEmail",this.ajaxService,true)
      break;
    }
    return validator
  }

  settFormGroupValidation(groupName,isUsedAccount,form){
    switch(groupName){
      case "CREDITCARD":
        isUsedAccount? '' : form.setValidators([this.CustomValidators.creditCardValidator(this.utils),this.CustomValidators.expCardData()]);
      break;
    }
  }

    makeTransaction(form,cashierDetailsForm,selectedMethod,txnType,cashierAttributesForm){
      let selectedMethodName = selectedMethod.providerName;
        if(form && form.valid && cashierDetailsForm.valid){
          let cashierData = this.utils.formControlToParams(form, {});

          cashierData = this.utils.formControlToParams(cashierDetailsForm, cashierData);
          cashierData["attributes"] = this.utils.formControlToParams(cashierAttributesForm, {});;
          if(selectedMethodName == "CREDITCARD"){
            if(typeof encryptData == "function"){
              form.controls["CreditcardNumber"] ? cashierData["encCreditcardNumber"] = encryptData(form.controls["CreditcardNumber"].value) : '';
              form.controls["cvv"] ? cashierData["encCvv"] = encryptData(form.controls["cvv"].value): '';
              delete cashierData["CreditcardNumber"];
              delete cashierData["cvv"];
              delete cashierData["expiryLabel"];
              delete cashierData["addNewCard"];
              delete cashierData["backToSavedCards"];
            }else{
              return {"status":"failed","reason":"encrptor not found","response":""}
            }
          }
          if(selectedMethod.serviceName){
            cashierData["service"] = selectedMethod.serviceName;
          }else{
            delete cashierData["service"]
          }
          return Promise.resolve(this.ajaxService.doTransaction(cashierData,txnType,selectedMethodName))
          .then(
            cashierResponse => {
              return {"status":"success","reason":"responseReceived","response":cashierResponse}
            }
          )
        }else{
          form ? this.utils.validateAllFormFields(form) : '';
          this.utils.validateAllFormFields(cashierDetailsForm);
          return {"status":"failed","reason":"form not valid","response":""}
        }
    }

    getSavedCards(cardDetails,userId){
      return Promise.resolve(this.ajaxService.getUserPaymentAccount(cardDetails,userId))
      .then(
        savedPaymentData =>{
          return savedPaymentData;
        }
      )
    }

}
