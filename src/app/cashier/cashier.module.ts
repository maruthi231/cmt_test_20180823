import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashierComponent } from './cashier.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { CashierService } from './services/cashier.service';
import { DepositService } from './services/deposit.service';
import { WithdrawService } from './services/withdraw.service';
import { QuickDepositFooterComponent } from './quick-deposit-footer/quick-deposit-footer.component';
import { SharedModule } from '../shared-modules/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule.forRoot()
  ],
  declarations: [
    CashierComponent,
    QuickDepositFooterComponent
  ],
  exports: [
    CashierComponent,
    QuickDepositFooterComponent
  ],
  providers: [
    CashierService,
    DepositService,
    WithdrawService
  ]
})
export class CashierModule { }
