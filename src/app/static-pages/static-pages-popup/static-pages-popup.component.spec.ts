import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPagesPopupComponent } from './static-pages-popup.component';

describe('StaticPagesPopupComponent', () => {
  let component: StaticPagesPopupComponent;
  let fixture: ComponentFixture<StaticPagesPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticPagesPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPagesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
