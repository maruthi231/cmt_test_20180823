import { Component, OnInit,SimpleChange, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Utility } from '../../utility/utility';
import * as $ from 'jquery';

@Component({
  selector: 'app-static-pages-popup',
  templateUrl: './static-pages-popup.component.html',
  styleUrls: ['./static-pages-popup.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class StaticPagesPopupComponent implements OnInit {

  @Input() url;
  @Output() staticPagePopupAction = new EventEmitter<any>();

  constructor(
    private utils:Utility
  ) { }

  ngOnInit() {
    this.utils.openModal('staticPagePopup',false);
    $("body").addClass("blur-bg");
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['url'] ? this.url = changes['url'].currentValue : '';
  }

  closeModal(modalId){
    this.utils.closeModal(modalId,false,true);
    this.staticPagePopupAction.emit("staticPagePopupClose");
  }

  ngOnDestroy(){
    $("body>#staticPagePopup").remove();
  }

}
