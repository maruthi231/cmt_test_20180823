import { Injectable } from '@angular/core';
import { AjaxService } from './ajax.service';
import { EmitterService } from './emitter.service';
import { Utility } from '../utility/utility';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import * as _ from 'underscore';

@Injectable()
export class GameService {
  gameAllData;
  indexedGameData;
  allFavGames;
  gameCallQueue = [];
  promiseQueue = [];
  cookieName = "_rpg"+environment.websiteCode;
  isForcePlay;
  isCalledOnGameOpen;
  constructor(
    private ajaxService:AjaxService,
    private emitterService:EmitterService,
    private utils:Utility,
    private router:Router
  ) { }

  getGameAllData(){
    return new Promise((resolve,reject)=>{
      if(this.gameCallQueue.length > 0){
          this.gameCallQueue.push(resolve)
        }else{
          this.gameCallQueue.push(resolve);

          if(!this.gameAllData){
            this.promiseQueue.push(Promise.resolve(this.ajaxService.getGames()));
          }
          this.promiseQueue.push(this.getFavouriteGames());
          Promise.all(this.promiseQueue).then(
            gamesAndFavData =>{
              var self = this;
              let gameData ;
              let favGameData;
              if(this.promiseQueue.length >= 2){
                gameData = gamesAndFavData [0];
                favGameData = gamesAndFavData [1];
                this.indexedGameData = _.indexBy(gameData,'pristineGameId');
              }else{
                gameData = this.gameAllData["rawGameData"];
                favGameData = gamesAndFavData [0];
                this.indexedGameData = this.indexedGameData;
              }

              this.allFavGames = this.formFavGameData(favGameData);
              this.gameAllData = {
                "rawGameData":gameData,
                "indexedGameData": this.indexedGameData,
                "allFavGames": this.allFavGames
              };
              for(let callback in  this.gameCallQueue){
                var resp_obj = JSON.parse(JSON.stringify(this.gameAllData));
                this.gameCallQueue[callback](resp_obj);
              }
              this.gameCallQueue = [];
              this.promiseQueue = [];
            }
          );

        }
    });
  }

  getFavouriteGames(){
    return Promise.resolve(this.ajaxService.getFavouriteGames())
  }

  getAvailableFavGames(){
    return this.allFavGames;
  }

  formFavGameData(favGameData){
    var self = this;
    if(favGameData && favGameData["favorite"] && favGameData["favorite"]["status"] == "SUCCESS" && favGameData["favorite"]["favoriteGamesList"] && favGameData["favorite"]["favoriteGamesList"].length > 0){
      this.allFavGames =  _.groupBy(favGameData["favorite"]["favoriteGamesList"],function(game){
        return game.gameId
      });
      this.allFavGames = _.each(this.allFavGames,function(game){
        if(self.indexedGameData[game[0].gameId]){
          self.allFavGames[game[0].gameId] = self.indexedGameData[game[0].gameId];
        }else{
          delete self.allFavGames[game[0].gameId];
        }
      })
    }else{
        this.allFavGames = [];
    }
    this.emitterService.broadcastFavGameUpdate("UPDATED");
    return this.allFavGames;
  }

  toggleFavouriteGame(gameFavData){
    return Promise.resolve(this.ajaxService.toggleFavouriteGames(gameFavData))
    .then(
      favGameData=>{
        if(favGameData){
          return this.getFavouriteGames();
        }
      },
      SystemError =>{

      }
    )
  }

  goToGame(gameCode,force){
    let lastPlayed;
    try{
      lastPlayed = atob(this.getCookie()).split(',');
    }catch(e){
      console.log(e);this.deleteAllRPGCookies();
    }
    let queryPrm = {}
    if(force){
      queryPrm = {"forceGame":"getFreeGames"}
    }
    this.router.navigate(["/en"+"/gamePlay/"+gameCode],{queryParams:queryPrm})

    // if(this.utils.isUserLoggedIn()){
    //   this.router.navigate(["/gamePlay/"+gameCode])
    //   //window.open(window.location.origin+"/gamePlay/gamelauncher/"+gameId)
    // }else{
    //   this.router.navigate(["/gamePlay/"+gameCode])
    //   //window.open(window.location.origin+"/gamePlay/freegames/"+gameId)
    // }
  }

  setLastPlayed(gameCode){
    let lastPlayed = ""
    try{
      lastPlayed = atob(this.getCookie());
    }catch(e){
      console.log(e);this.deleteAllRPGCookies();
    }
    if(!lastPlayed.includes(gameCode)){
      lastPlayed = gameCode+","+lastPlayed;
      let lastPlayedArray = lastPlayed.split(",").slice(0,9).join(",");
      this.setCookie(btoa(lastPlayedArray));
      this.emitterService.broadcastLastPlayedUpdated("LastPlayUpdated");
    }
  }

  getAllLastPlayed(){
    let allLastPlayed = [];
    try{
      allLastPlayed = atob(this.getCookie()).split(",");
    }catch(e){
      console.log(e);this.deleteAllRPGCookies();
    }finally{
      return allLastPlayed;
    }

  }

  getCookie(){
    var name = this.cookieName + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  setCookie(cookieData){
    var d = new Date();
    d.setTime(d.getTime() + (2592000));
    var expires = "expires="+d.toUTCString();
    document.cookie = this.cookieName + "=" + cookieData + ";" + expires + ";path=/";
  }

  deleteAllRPGCookies(){
    document.cookie = this.cookieName+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  }
}
