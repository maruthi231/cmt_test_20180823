import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import { availableCountries } from '../utility/config';

@Injectable()
export class AppDetailsService {

  appAvailableCountries;
  appAvailableCurrencies;

  constructor() { }

  setAvailableCountries(countries){
    let availableCountriesList = []
    _.each(countries.countrylist,function(isoCountry){
      if(_.contains(availableCountries,isoCountry.iso)){
        availableCountriesList.push(isoCountry)
      }
    })
    countries["countrylist"] = availableCountriesList
    this.appAvailableCountries = countries;
  }

  getAvailableCountries(){
    return this.appAvailableCountries;
  }

  setAvailableCurrencies(currencies){
    this.appAvailableCurrencies = currencies;
  }

  getAvailableCurrencies(){
    return this.appAvailableCurrencies;
  }

}
