import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamePlayComponent } from './game-play.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: GamePlayComponent
      }
    ])
  ],
  exports: [RouterModule]
})
export class GameRouteModule { }
