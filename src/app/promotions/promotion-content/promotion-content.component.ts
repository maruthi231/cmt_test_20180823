import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PromotionsService } from '../../services/promotions.service';
import { environment } from '../../../environments/environment';
import { EmitterService } from '../../services/emitter.service';
import { Utility } from '../../utility/utility';
import { Router } from '@angular/router';
import { UserDetailsService } from '../../services/user-details.service';

import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-promotion-content',
  templateUrl: './promotion-content.component.html',
  styleUrls: ['./promotion-content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PromotionContentComponent implements OnInit {
  isPageAvailable;
  loader = true;
  loginSubs;
  logoutSubs;
  cashierTransactionSuccess;
  constructor(
    private utils:Utility,
    private router:Router,
    private emitterService:EmitterService,
    private promotionsService:PromotionsService,
    private activatedRoute:ActivatedRoute,
    private userDetailsService: UserDetailsService,
  ) {
    this.loginSubs = this.emitterService.loginComplete$.subscribe(
      loginStatus => {
        if (loginStatus == "LoginCompleted") {
          this.setPromoContent(true);
        }
      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.setPromoContent(true);
        }
      }
    )
    this.cashierTransactionSuccess = this.emitterService.getBannerAndPromo$.subscribe(
      transactionSuccessData => {
        if (transactionSuccessData == "TimeOutOver") {
          var self = this;
            self.setPromoContent(true);
        }
      }
    )
  }

  ngOnInit() {
    this.setPromoContent(false)
  }

  setPromoContent(isForce){
    var self = this;
    this.activatedRoute.params.subscribe(
      params=>{
        let promoUrl = "promotions/"+params["promoUrl"];
        this.loader = true;
        Promise.resolve(this.promotionsService.getIndexedPromotionData(isForce))
        .then(
          promotionsData =>{
            this.loader = false;

            if(promotionsData && promotionsData[promoUrl] && promotionsData[promoUrl]["content"]){
              let content  = promotionsData[promoUrl].content.replace(new RegExp('{{site_url}}','g'), environment.siteUrl);
              this.isPageAvailable = true;
              setTimeout(function(){
                $("#promoContent").html(content);
                setTimeout(function(){
                  self.utils.elementsChange();
                })
                self.utils.setSEO(promotionsData[promoUrl],false);
                window["prerenderReady"] = true;
                $("#promoContent").on('click',function(e){
                  let routerLink = $(e.target).attr("routerLink");
                  if(routerLink){
                    self.utils.cmsRouter(routerLink);
                  }
                })
              })
            }else{
              this.isPageAvailable = false;
            }
          }
        )
      }
    )
  }



  ngOnDestroy(){
    this.utils.setSEO("",false);
    this.loginSubs.unsubscribe();
    this.logoutSubs.unsubscribe();
    this.cashierTransactionSuccess.unsubscribe();
  }

}
