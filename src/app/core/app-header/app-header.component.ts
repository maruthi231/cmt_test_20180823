import { Component, OnInit, ViewEncapsulation, Input, SimpleChange } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import { GameService } from '../../services/game.service';
import { EmitterService } from '../../services/emitter.service';
import { UserDetailsService } from '../../services/user-details.service';
import { Utility } from '../../utility/utility';
import { Router } from '@angular/router';
import { SocketService } from '../../shared-modules/services/socket.service';
import * as $ from 'jquery';
// import 'bigslide';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class AppHeaderComponent implements OnInit {
  @Input() isGameLaunch;
  @Input() resetPasswordToken;
  @Input() resetPassword;
  @Input() redirectTo;
  @Input() windowLocation;
  isUserLoggedIn: boolean = undefined;
  userNickName: String = "";
  userCurrencySymbol;
  userCashBalance;
  userTotalBalance;
  logoutSubscription;
  menuItemClicked = undefined;
  cashierClicked = undefined;
  resetPasswordOpened = false;
  hideHeader;
  hideHeaderStatus = 'show';
  modalCloseSubs;
  sub;
  userBalanceSubscription;
  staticPageUrl;
  staticPageUrlSubs;
  leftMenuOpenend;
  userSessionAvailable;
  cashierSubs;
  logoutSubs;
  constructor(
    private ajaxService: AjaxService,
    private userDetailsService: UserDetailsService,
    private utils: Utility,
    private emitterService: EmitterService,
    private router: Router,
    private gameService: GameService,
    private socketService:SocketService
  ) {
    this.logoutSubscription = this.emitterService.doLogoutComplete$.subscribe(logoutStatus => {
      if (logoutStatus.includes("LOGOUT_USER_CLOSE_MODAL")) {
        this.doLogout(logoutStatus);
      }
    })

    this.hideHeader = this.emitterService.hideheader$.subscribe(hideHeaderStatus => {
      this.hideHeaderStatus = hideHeaderStatus;
    })

    this.modalCloseSubs = this.emitterService.modalClose$.subscribe(
      modalId =>{
        if(modalId == "settingsModal"){
          this.settingsClosed("");
        }
        if(modalId == "staticPagePopup"){
          this.staticPagePopupAction("staticPagePopupClose");
        }
      }
    )

    this.userBalanceSubscription = this.userDetailsService.userBalanceUpdated$.subscribe(
      balanceUpdateMessage => {
        if(balanceUpdateMessage == "UserBalanceUpdated"){
          this.userCashBalance = this.userDetailsService.getUserCashBalance();
          this.userTotalBalance = this.userDetailsService.getUserTotalBalance();
        }
      }
    );

    this.staticPageUrlSubs = this.emitterService.staticPageUrl$.subscribe(staticPageUrl => {
      this.staticPageUrl = staticPageUrl;
    })

    this.cashierSubs = this.emitterService.openCashier$.subscribe(
      cashierUrl => {
        if(cashierUrl)
        this.cashierClicked = cashierUrl;

      }
    )

    this.logoutSubs = this.emitterService.logoutComplete$.subscribe(
      logoutStatus => {
        if (logoutStatus == "LogoutCompleted") {
          this.userSessionAvailable = false;
          this.cashierClicked = undefined;
          this.menuItemClicked = undefined;
          sessionStorage.removeItem("user");
          sessionStorage.removeItem("session");
          this.gameService.getGameAllData();
          this.isUserLoggedIn = false;
        }
      }
    )

  }

  ngOnInit() {
    Promise.resolve(this.ajaxService.getLoginStatus())
      .then(data => {
        this.socketService.setSocketUrl(data["riverplayUrl"]);
        this.socketService.setPartnerId(data["partnerId"]);
        this.socketService.setSessionId(data["rvpSessionId"]);
        this.emitterService.broadcastStatusCallDone("statusCallSuuccess");
        if (data && data["status"] == true) {
          this.userSessionAvailable = true;
          this.getUserDetails({message:"",rvpSessionId:data["rvpSessionId"],redirectTo:this.redirectTo,socketUrl:data["riverplayUrl"],partnerID:data["partnerId"]});
        } else {
          sessionStorage.removeItem("user");
          sessionStorage.removeItem("session");
          this.isUserLoggedIn = false;
        }

      }, SystemError => {
        sessionStorage.removeItem("user");
        sessionStorage.removeItem("session");
        this.isUserLoggedIn = false;
      });
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['resetPasswordToken'] && changes['resetPasswordToken'].currentValue ? this.resetPasswordToken = changes['resetPasswordToken'].currentValue : '';
    changes['resetPassword'] && changes['resetPassword'].currentValue ? this.resetPassword = changes['resetPassword'].currentValue : '';
    changes['isGameLaunch'] && changes['isGameLaunch'].currentValue ? this.isGameLaunch = changes['isGameLaunch'].currentValue : '';
    changes['redirectTo'] && changes['redirectTo'].currentValue ? this.redirectTo = changes['redirectTo'].currentValue : '';
    changes['windowLocation'] && changes['windowLocation'].currentValue ? this.windowLocation = changes['windowLocation'].currentValue : '';

    if(this.resetPassword && this.resetPasswordToken && !this.resetPasswordOpened){
      this.resetPasswordOpened = true;
      var self = this;
      setTimeout(function(){
        self.utils.openModal("resetPassword",true);
      },1)
    }
  }

  getUserDetails(totalData) {
    var self = this;
    let message  = totalData.message;
    let rvpSessionId = totalData.rvpSessionId;
    let socketUrl = totalData.socketUrl;
    let partnerID = totalData.partnerID;
    this.socketService.setSocketUrl(socketUrl);
    this.socketService.setPartnerId(partnerID);
    this.socketService.setSessionId(rvpSessionId);
    this.sub = this.socketService.connectToScoket("authCode");
    if (!message || (message.includes('Registration') && !message.includes('Failed')) || !message.includes('Registration')) {
      Promise.resolve(this.ajaxService.getProfileBalanceCurrency())
        .then(profileBalanceCurrency => {
          let obj = {}
          if (profileBalanceCurrency) {
            this.userDetailsService.setUserCashBalance(profileBalanceCurrency["profile"]["balanceDetails"]["cash"]);
            this.userDetailsService.setUserBalanceDetails(profileBalanceCurrency["profile"]["balanceDetails"]);
            this.userDetailsService.setUserBonusBalance(profileBalanceCurrency["profile"]["balanceDetails"]["bonus"]);
            this.userDetailsService.setUserTotalBalance(profileBalanceCurrency["profile"]["balanceDetails"]["cash"] + profileBalanceCurrency["profile"]["balanceDetails"]["bonus"]);
            this.userDetailsService.setUserProfileDetails(profileBalanceCurrency["profile"]);
            this.userDetailsService.setUserCurrencyDetails(profileBalanceCurrency["currency"]);
            this.userNickName = profileBalanceCurrency["profile"]["nickname"];
            sessionStorage.setItem("_player",btoa(profileBalanceCurrency["profile"]["playerID"]))
            if (message.includes('LoginSuccess/') || message.includes('Registration/'))
              this.utils.enableButton(message.split("/")[1], "");
            if (message.includes('Registration')) {
              this.utils.closeModal("signUpModal", true, true)
            }
            this.userCurrencySymbol = this.userDetailsService.getUserCurrencyDetails().symbol;
            this.userCashBalance = this.userDetailsService.getUserCashBalance();
            this.userTotalBalance = this.userDetailsService.getUserTotalBalance();
            sessionStorage.setItem("user", "true");
            message ? this.emitterService.broadcastloginComplete("LoginCompleted") : '';
            this.utils.closeModal("signUpModal", false, true);
            this.utils.closeModal("loginmodal", false, false);
            if(this.gameService.isCalledOnGameOpen){
              this.gameService.isCalledOnGameOpen = false;
              setTimeout(function(){
                self.emitterService.broadcastOpenGamePopUp("openGamePopup")
              },1)
            }
            this.isUserLoggedIn = true;
            if(totalData.redirectTo){
              this.cashierClicked = "transaction"
            }
          }
        });
      if (message && !message.includes('Failed')) {
        this.gameService.getGameAllData();
      }
    }
  }

  doLogout(actionAfterLogout) {
    Promise.resolve(this.ajaxService.doLogout())
      .then(
        loginData => {
          this.emitterService.broadcastlogoutComplete("LogoutCompleted")
          if (actionAfterLogout.includes("LOGOUT_USER_CLOSE_MODAL")) {
            this.utils.closeModal(actionAfterLogout.split(":")[1], false, true)
          }
        },
        SystemError => {}
      )

  }

  openLoginModal() {
    this.utils.openModal("loginModal", true)
  }

  settingsClosed(message) {
    if(this.menuItemClicked){
      this.utils.closeModal("settingsModal",false,true)
      this.menuItemClicked = undefined;
    }
    if(message.includes("OpenCashier")){
      let messageArr = message.split("-");
      if(message.split("-").length > 1){
        this.cashierClicked = message.split("-")[1]
      }
    }
  }

  cashierClosed(txnType) {
    let action = txnType.split("/")
    if (action[0])
      this.cashierClicked = undefined;
    if (action[1] && action[1] == "openTransactions")
      this.menuItemClicked = "transactionDetails";
  }

  checkForExpand(e){
    this.leftMenuOpenend = this.utils.checkForExpand(this.leftMenuOpenend,e)
  }

  ngAfterViewInit() {
    var self = this;
    $(document).ready(function() {
      // open side menu and push all component to right

        $('#side-menu-btn').on('click', function(e) {
            $('.side-menu').toggleClass("side-menu-opened");
            $('.push-side').toggleClass("side-menu-opened");
            $('body').toggleClass("backdrop-mask");
            e.stopPropagation();
        });
        $('.black-mask').on('click', function(e) {
            $('.side-menu').toggleClass("side-menu-opened");
            $('.push-side').toggleClass("side-menu-opened");
            $('body').toggleClass("backdrop-mask");
            e.stopPropagation();
        });

        $('.expandable-menu').on('click',function(e){
          self.checkForExpand(e);
        })

        $('.icon-plus','.icon-minus').on('click',function(e){
          self.checkForExpand(e);
        })



      if(window.pageYOffset > 0){
        $(".main-header ").addClass("active");
      }
// ON scroll top header wll be added bg color
      $(function() {
        $(window).on("scroll", function() {
          if ($(window).scrollTop() > 0) {
            $(".main-header ").addClass("active");
          } else {
             $(".main-header ").removeClass("active");
          }
        });
      });


    });
  }

  addModalClass(side) {

  }

  hideLeftMenuAndOpen(page) {
    if (page == 'login') {
      this.openLoginModal();
    } else if (page == 'logout') {
      this.doLogout('');
      this.router.navigate(["/en"]);
    } else if (page == 'myDetails' ||
      page == 'balanceDetails' ||
      page == 'bonusDetails' ||
      page == 'transactionDetails' ||
      page == 'kycDetails') {
      this.menuItemClicked = undefined;
      this.menuItemClicked = page;
    } else {
      this.router.navigate([page+"/en"]);
    }
    $('body').removeClass('small-screen-push-left').removeClass('small-screen-push-right');
  }

  stopPropogation($event){
    $event.stopPropagation();
  }

  staticPagePopupAction(message){
    if(message == "staticPagePopupClose")
      this.staticPageUrl = undefined;
  }

  showFavourites(){
    this.emitterService.broadcastShowFavouriteGames("showFavourites");
  }

  ngOnDestroy(){
    this.logoutSubscription.unsubscribe();
    this.hideHeader.unsubscribe();
    this.sub.unsubscribe();
    this.userBalanceSubscription.unsubscribe();
    this.cashierSubs.unsubscribe();
    this.logoutSubs.unsubscribe();
  }

}
