import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JkrLoaderComponent } from './jkr-loader.component';

describe('JkrLoaderComponent', () => {
  let component: JkrLoaderComponent;
  let fixture: ComponentFixture<JkrLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JkrLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JkrLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
