import { NgModule, ModuleWithProviders } from '@angular/core';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { JkrLoaderComponent } from './jkr-loader/jkr-loader.component';

@NgModule({
  imports: [
    RouterModule
  ],
  declarations: [
    PageNotFoundComponent,
    FormValidationComponent,
    JkrLoaderComponent
  ],
  exports: [
    PageNotFoundComponent,
    JkrLoaderComponent
  ],
  providers: [

  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [  ]
    };
  }
}
