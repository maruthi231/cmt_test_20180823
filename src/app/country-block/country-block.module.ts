import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryBlockComponent } from './country-block.component';

@NgModule({
  declarations: [CountryBlockComponent],
  exports:[CountryBlockComponent]
})
export class CountryBlockModule { }
