import { Component, OnInit, Input, SimpleChange,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {

  @Input() balanceDetails;
  @Input() profileDetails;
  @Output() openCashier = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}): void {
    changes['balanceDetails'] && changes['balanceDetails'].currentValue ? this.balanceDetails = changes['balanceDetails'].currentValue: '';
    changes['profileDetails'] && changes['profileDetails'].currentValue ? this.profileDetails = changes['profileDetails'].currentValue: '';
  }

  openCasiher(type){
    this.openCashier.emit(type)
  }

}
