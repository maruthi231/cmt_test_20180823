import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import { AjaxService } from '../../services/ajax.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.scss']
})
export class BonusComponent implements OnInit {

  @Input() profileDetails;
  @Input() currencyDetails;
  type;
  activeBonusList;
  bonusHistoryList;

  constructor(
    private ajaxService:AjaxService
  ) { }

  ngOnInit() {
    this.type = 'active';
    this.getBonusDetails(this.type);
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['profileDetails'] && changes['profileDetails'].currentValue ? this.profileDetails = changes['profileDetails'].currentValue : '';
    changes['currencyDetails'] && changes['currencyDetails'].currentValue ? this.currencyDetails = changes['currencyDetails'].currentValue : '';
  }

  getBonusDetails(type){
    switch(type){
      case 'active':
        this.activeBonusList = undefined;
        Promise.resolve(this.ajaxService.getActiveBonusForUser())
        .then(
          activeBonusList =>{
            if(activeBonusList){
              let bonuslist = [];
              _.each(activeBonusList,function(bonus){
                if(bonus.isFreeSpinBonus && bonus.isFreeSpinsCompleted){
                  bonuslist.push(bonus)
                }else if(!bonus.isFreeSpinBonus){
                  bonuslist.push(bonus)
                }
              })
              this.activeBonusList = bonuslist;
            }
            else
              this.activeBonusList = []
          }
        )
      default:
      break;
      case 'history':
      this.bonusHistoryList = undefined;
      Promise.resolve(this.ajaxService.getHistoryBonusForUser())
      .then(
        historyBonusList =>{
          if(historyBonusList && historyBonusList["ecrBonusDataList"] && historyBonusList["ecrBonusDataList"].length > 0){
            let bonuslist = [];
            _.each( historyBonusList["ecrBonusDataList"],function(bonus){
              if(bonus.isFreeSpinBonus && bonus.isFreeSpinsCompleted){
                bonuslist.push(bonus)
              }else if(!bonus.isFreeSpinBonus){
                bonuslist.push(bonus)
              }
            })
            this.bonusHistoryList = bonuslist;
          }
          else
            this.bonusHistoryList = []
        }
      )
      break;
    }
  }

}
