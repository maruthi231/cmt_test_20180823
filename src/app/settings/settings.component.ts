import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utility } from '../utility/utility';
import { AjaxService } from '../services/ajax.service';
import { AppDetailsService } from '../services/app-details.service';
import { CustomValidators } from '../utility/custom-validator';
import { UserDetailsService } from '../services/user-details.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
      encapsulation: ViewEncapsulation.None

})
export class SettingsComponent implements OnInit {
  @Input() menuItemClicked;
  @Output() settingsClosed = new EventEmitter<string>();
  profileDetails;
  balanceDetails;
  userProfileSubscription;
  userBalanceSubscription;
  kycDetails;
  userKycSubscription;
  userCurrencySubscription;
  currencyDetails;
  eligibleBonusList;
  constructor(
    private utils: Utility,
    private ajaxService: AjaxService,
    private formBuilder: FormBuilder,
    private appDetails:AppDetailsService,
    private CustomValidators:CustomValidators,
    private userDetailsService:UserDetailsService
  ) {
    this.userProfileSubscription = this.userDetailsService.userProfileUpdated$.subscribe(
      profileUpdateMessage => {
        if(profileUpdateMessage == "UserProfileUpdated"){
          this.getProfileDetails();
        }
      }
    );

    this.userBalanceSubscription = this.userDetailsService.userBalanceUpdated$.subscribe(
      balanceUpdateMessage => {
        if(balanceUpdateMessage == "UserBalanceUpdated"){
          this.getBalanceDetails(false);
        }
      }
    );

    this.userKycSubscription = this.userDetailsService.userKycUpdated$.subscribe(
      kycUpdateMessage => {
        if(kycUpdateMessage == "UserKycUpdated"){
          this.getKycDetails();
        }
      }
    );

    this.userCurrencySubscription = this.userDetailsService.userCurrencyUpdated$.subscribe(
      currencyUpdateMessage => {
        if(currencyUpdateMessage == "UserCurrencyUpdated"){
          this.getProfileDetails();
        }
      }
    );
  }

  ngOnInit() {
    this.changeMenuItemClicked(this.menuItemClicked);
    this.openModal("settingsModal");
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
      let prevMenu = this.menuItemClicked
      changes['menuItemClicked'] && changes['menuItemClicked'].currentValue ? this.menuItemClicked = changes['menuItemClicked'].currentValue : '';
      if(prevMenu != this.menuItemClicked)
        this.changeMenuItemClicked(this.menuItemClicked);
  }

  changeMenuItemClicked(menuItem){
    this.menuItemClicked = menuItem;
    switch(this.menuItemClicked){
      case "myDetails":
        this.getProfileDetails();
      break;
      case "balanceDetails":
        this.getBalanceDetails(true);
      break;
        case "transactionDetails":
        this.getProfileDetails();
      break;
      case "bonusDetails":
        //this.getBonusDetails();
        this.getProfileDetails();
      break;
      case "kycDetails":
        this.getKycDetails();
      break;
      default:
      break;
    }
  }

  getProfileDetails(){
    if(this.userDetailsService.getUserProfileDetails() && this.userDetailsService.getUserCurrencyDetails()){
      this.profileDetails = this.userDetailsService.getUserProfileDetails();
      this.currencyDetails = this.userDetailsService.getUserCurrencyDetails();
    }else{
      this.getProfileBalanceCurrency();
    }
  }

  getBalanceDetails(force){
    if(this.userDetailsService.getUserBalanceDetails() && this.userDetailsService.getUserProfileDetails() && !force){
      this.profileDetails = this.userDetailsService.getUserProfileDetails();
      this.balanceDetails = this.userDetailsService.getUserBalanceDetails();
    }else{
      this.profileDetails = undefined;
      this.balanceDetails = undefined;
      //this.getProfileBalanceCurrency();
      this.userDetailsService.forceGetUserProfileDetails();
    }
  }

  getBonusDetails(){
    // Promise.resolve(this.ajaxService.getEligibleBonuses())
    // .then(
    //   eligibleBonusData => {
    //     if(eligibleBonusData["status"] == "SUCCESS" && eligibleBonusData["eligibleBonusList"].length > 0){
    //       this.eligibleBonusList = eligibleBonusData["eligibleBonusList"];
    //     }else{
    //
    //     }
    //   }
    // );
  }


  getKycDetails(){
    // if(this.userDetailsService.getUserKycDetails()){
    //   this.kycDetails = this.userDetailsService.getUserKycDetails();
    // }else{
      this.getAccountVerificationStatus();
    //}
  }

  getAccountVerificationStatus(){
    Promise.resolve(this.ajaxService.getAccountVerificationStatus())
    .then(
      verificationData => {
        if(verificationData && verificationData["documents"]){
          this.kycDetails = verificationData["documents"];
          //this.userDetailsService.setUserKycDetails(verificationData["documents"]);
        }else{

        }
      }
    );
  }

  getProfileBalanceCurrency(){
    Promise.resolve(this.ajaxService.getProfileBalanceCurrency())
    .then(profileBalanceCurrency => {
      if (profileBalanceCurrency) {
        this.userDetailsService.setUserCashBalance(profileBalanceCurrency["profile"]["balanceDetails"]["cash"]);
        this.userDetailsService.setUserBalanceDetails(profileBalanceCurrency["profile"]["balanceDetails"]);
        this.userDetailsService.setUserBonusBalance(profileBalanceCurrency["profile"]["balanceDetails"]["bonus"]);
        this.userDetailsService.setUserTotalBalance(profileBalanceCurrency["profile"]["balanceDetails"]["cash"] + profileBalanceCurrency["profile"]["balanceDetails"]["bonus"]);
        this.userDetailsService.setUserProfileDetails(profileBalanceCurrency["profile"]);
        this.userDetailsService.setUserCurrencyDetails(profileBalanceCurrency["currency"]);
      }
    });
  }

  openModal(modalId){
    this.utils.openModal(modalId,"");
  }

  closeModal(modalId){
    this.utils.closeModal(modalId,"",true);
    this.settingsClosed.emit("closeModal:settingsModal");
  }

  openCashier(type){
    this.settingsClosed.emit("closeModal:settingsModal/OpenCashier-"+type);
  }

  ngOnDestroy(){
    this.userProfileSubscription.unsubscribe();
    $("body>#settingsModal").remove();
  }

}
