import { Component, OnInit, Input, SimpleChange, ViewEncapsulation, EventEmitter, Output} from '@angular/core';
import { transactionTypeConfs } from '../../utility/config';
import { AjaxService } from '../../services/ajax.service';

import * as $ from 'jquery';
import 'intl-tel-input';
import 'bootstrap-select';


@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class AppTransactions implements OnInit {
  @Input() profileDetails;
  @Input() currencyDetails;
  transactionDetails;
  request;
  constructor(
    private ajaxService: AjaxService,
  ) {}

  ngOnInit() {
    this.getTransaction("cashier");
    $('select').selectpicker({});

  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['profileDetails'] ? this.profileDetails = changes['profileDetails'].currentValue : '';
    changes['currencyDetails'] ? this.currencyDetails = changes['currencyDetails'].currentValue : '';
  }

  setRequestInitialData(type){
    let startDate = new Date;
    this.request = {
      index: 0,
      size: 10,
      order: true,
      transactionTypes:type == "cashier" ? transactionTypeConfs : [],
      interval:{
        start:new Date(startDate.setMonth(startDate.getMonth()-1)),
        end:new Date(),
      }
    }
    if(type == "casino") {
      this.request["type"] = type;
    };
  }

  getTransactionDetails(request){
    this.transactionDetails = undefined;
    if(request) this.request = request;
    Promise.resolve(this.ajaxService.getUserTransactionsHistory(this.request))
    .then(transactionData => {
      this.transactionDetails = transactionData;
    });
  }

  getTransaction(type){
    this.setRequestInitialData(type);
    this.getTransactionDetails("");
  }


  changeDate(){

  }

}
