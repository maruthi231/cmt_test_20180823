import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CustomValidators } from '../../utility/custom-validator';
import { AjaxService } from '../../services/ajax.service';
import { AppDetailsService } from '../../services/app-details.service';
import { Utility } from '../../utility/utility';
import { UserDetailsService } from '../../services/user-details.service';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

import * as _ from 'underscore';
import * as $ from 'jquery';
import 'intl-tel-input';
import 'bootstrap-select';

@Component({
  selector: 'app-my-profile-genral',
  templateUrl: './my-profile-genral.component.html',
  styleUrls: ['./my-profile-genral.component.scss']
})
export class MyProfileGenralComponent extends FormValidationComponent implements OnInit {
  yearRange: any = [];
  availableCountries;
  @Input() profileDetails;
  countryData;
  focusedElement;
  serverResponse;
  updateProfileForm = this.formBuilder.group({
    'email': ['', [this.CustomValidators.validateUniqueness("txtEmail", this.ajaxService, false)]],
    'nickname': ['', [this.CustomValidators.validateUniqueness("txtNickname", this.ajaxService, false)]],
    'areaCode': ['', this.CustomValidators.required],
    'areaCountryCode': ['', this.CustomValidators.required],
    'phone': ['', [this.CustomValidators.phoneNumberValidator]],
    'mobilePhone': ['', [this.CustomValidators.phoneNumberValidator]],
    'mobileAreaCode': ['', this.CustomValidators.required],
    'mobileAreaCountryCode': ['', this.CustomValidators.required],
    'firstName': ['', [this.CustomValidators.validName(2, 30)]],
    'lastName': ['', [this.CustomValidators.validName(2, 30)]],
    'gender': ['M', [this.CustomValidators.required]],
    'bDay': ['', [this.CustomValidators.required]],
    'bMonth': ['', [this.CustomValidators.required]],
    'bYear': ['', [this.CustomValidators.required]],
    'address1': ['', [this.CustomValidators.addressValidator(false)]],
    'address2': ['', [this.CustomValidators.addressValidator(true)]],
    'city': ['', [this.CustomValidators.validName(2, 50)]],
    'zip': ['', [this.CustomValidators.validAlphaNumeric(2, 9)]],
    'country': ['', [this.CustomValidators.required]],
    'state': ['-NA-', [this.CustomValidators.required]]
  }, {validator:this.CustomValidators.dobValidator} );

  constructor(
    private formBuilder: FormBuilder,
    private ajaxService: AjaxService,
    private appDetails: AppDetailsService,
    private utils: Utility,
    private CustomValidators: CustomValidators,
    private userDetailsService: UserDetailsService
  ) {
    super(utils);
  }

  ngOnInit() {
    this.showPreferences("","");
    this.yearRange = this.yearRange.length > 0 ? this.yearRange : _.sortBy(_.range(1900, new Date().getFullYear() - 17), function(num) { return -num; });
    let avCountryData = this.appDetails.getAvailableCountries();
    if (avCountryData) {
      this.countryData = avCountryData;
      this.availableCountries = avCountryData["countrylist"];
    } else {
      Promise
        .all([
          Promise.resolve(this.ajaxService.getCountries()),
          Promise.resolve(this.ajaxService.getEnabledCuurencies())
        ])
        .then(
          countryAndCurrencyData => {
            this.appDetails.setAvailableCountries(countryAndCurrencyData[0]);
            this.appDetails.setAvailableCurrencies(countryAndCurrencyData[1]);
            this.countryData = countryAndCurrencyData[0];
            let currencyData = countryAndCurrencyData[1];
            this.availableCountries = this.countryData["countrylist"];
            if (this.profileDetails && this.countryData) {
              this.setFormValues();
            }
          },
          SystemError => {

          }
        );
    }
    if (this.profileDetails) {
      this.setFormValues();
    }
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['profileDetails'] && changes['profileDetails'].currentValue ? this.profileDetails = changes['profileDetails'].currentValue : '';
    if (this.profileDetails && !changes['profileDetails']["firstChange"]) {
      this.setFormValues();
    }
  }

  setFormValues() {
    var self = this;
    this.profileDetails.email ? this.updateProfileForm.controls["email"].setValue(this.profileDetails.email) : '';
    this.profileDetails.nickname ? this.updateProfileForm.controls["nickname"].setValue(this.profileDetails.nickname) : '';
    this.profileDetails.firstName ? this.updateProfileForm.controls["firstName"].setValue(this.profileDetails.firstName) : '';
    this.profileDetails.lastName ? this.updateProfileForm.controls["lastName"].setValue(this.profileDetails.lastName) : '';
    this.profileDetails.bDay ? this.updateProfileForm.controls["bDay"].setValue(Number(this.profileDetails.bDay)) : '';
    this.profileDetails.bMonth ? this.updateProfileForm.controls["bMonth"].setValue(Number(this.profileDetails.bMonth)) : '';
    this.profileDetails.bYear ? this.updateProfileForm.controls["bYear"].setValue(Number(this.profileDetails.bYear)) : '';
    this.profileDetails.address1 ? this.updateProfileForm.controls["address1"].setValue(this.profileDetails.address1) : '';
    this.profileDetails.address2 ? this.updateProfileForm.controls["address2"].setValue(this.profileDetails.address2) : '';
    this.profileDetails.city ? this.updateProfileForm.controls["city"].setValue(this.profileDetails.city) : '';
    this.profileDetails.country ? this.updateProfileForm.controls["country"].setValue(this.profileDetails.country) : '';
    this.profileDetails.zip ? this.updateProfileForm.controls["zip"].setValue(this.profileDetails.zip) : '';

    this.profileDetails.cellPhone ? this.updateProfileForm.controls["mobilePhone"].setValue(this.profileDetails.cellPhone) : '';
    this.profileDetails.cellPhoneCountryAreaCode ? this.updateProfileForm.controls["mobileAreaCountryCode"].setValue(this.profileDetails.cellPhoneCountryAreaCode) : '';
    this.profileDetails.cellPhoneAreaCode ? this.updateProfileForm.controls["mobileAreaCode"].setValue(this.profileDetails.cellPhoneAreaCode) : '';

    this.profileDetails.phone ? this.updateProfileForm.controls["phone"].setValue(this.profileDetails.phone) : '';
    this.profileDetails.countryAreaCode ? this.updateProfileForm.controls["areaCountryCode"].setValue(this.profileDetails.countryAreaCode) : '';
    this.profileDetails.areaCode ? this.updateProfileForm.controls["areaCode"].setValue(this.profileDetails.areaCode) : '';

    this.profileDetails.gender ? this.updateProfileForm.controls["gender"].setValue(this.profileDetails.gender) : this.updateProfileForm.controls["gender"].setValue("M");
    if (this.profileDetails && this.countryData) {
      setTimeout(function() {
        self.utils.setPhoneNumberDropdown("mobileNumberFlag", self.profileDetails.cellPhoneCountryAreaCode ? self.profileDetails.cellPhoneCountryAreaCode : self.countryData["country"], [], self.updateProfileForm);
        self.utils.setPhoneNumberDropdown("phoneNumberFlag", self.profileDetails.countryAreaCode ? self.profileDetails.countryAreaCode : self.countryData["country"], [], self.updateProfileForm);
        $('select').selectpicker({});
      },10)

    }
  }

  updateProfile(buttonId) {
    this.serverResponse = "";
    if (this.updateProfileForm.valid) {
      this.utils.disableButton(buttonId,"please wait...",true);
      let updateProfileData = this.utils.formControlToParams(this.updateProfileForm, {});
      updateProfileData["birthDate"] = updateProfileData['bYear'] + '-' + updateProfileData['bMonth'] + '-' + updateProfileData['bDay'];
      Promise.resolve(this.ajaxService.doProfileUpdate(updateProfileData))
        .then(
          updateProfileResponse => {
            if (updateProfileResponse["success"] && updateProfileResponse["success"]["status"] === "SUCCESS") {
              Promise.resolve(this.ajaxService.getProfileData())
                .then(profileDetails => {
                  if (profileDetails) {
                    this.userDetailsService.setUserProfileDetails(profileDetails);
                  }
                });
              this.serverResponse = "UPDATE_PROFILE_SUCCESS";
              this.utils.enableButton(buttonId,"UPDATE PROFILE");
            } else if (updateProfileResponse["errors"][0] && updateProfileResponse["errors"][0]["message"]) {
              this.serverResponse = updateProfileResponse["errors"][0]["message"];
              this.utils.enableButton(buttonId,"UPDATE PROFILE");
            }else if(updateProfileResponse["errors"]){
              this.serverResponse = "Please check all the fields for valid data";
              this.utils.enableButton(buttonId,"UPDATE PROFILE");
            }else{
              this.serverResponse = "Something went wrong. Please try again";
              this.utils.enableButton(buttonId,"UPDATE PROFILE");
            }
          },
          SystemError => {}
        )
    } else {
      this.utils.validateAllFormFields(this.updateProfileForm);
    }
  }

  updatePreference(selectedPref){
     $("#"+selectedPref).parent().children("label").addClass("loader-s");
     this.utils.disableButton("updateProfileButton","UPDATE PROFILE",false);

     $("input[type=checkbox]").attr('disabled', true);
    let data = {
      "emailSubscribed":($('#sendOffers:checked').length > 0 ?true:false),
      "mobileSubscribed":($('#sendOffersSms:checked').length > 0?true:false)
    }

    this.ajaxService.updateSubscriptionPreferenes(data)
    .then(prefResposne=>{
        if(prefResposne["status"] == "SUCCESS"){
          this.showPreferences("SUCCESS",selectedPref);
        }else{
          this.showPreferences("FAILIURE",selectedPref);
        }
    },SystemError => {
      this.showPreferences("FAILIURE",selectedPref);
    })
    .catch(error => {});
   }

   showPreferences(status,selectedCheckbox){
    this.ajaxService.getSubscriptionPreferenes()
    .then(prefResposne=>{
      if(prefResposne){
        $('#sendOffersSms').attr('checked',prefResposne["mobileSubscribed"]);
        $('#sendOffers').attr('checked',prefResposne["emailSubscribed"]);
      }
      if(selectedCheckbox){
        $("#"+selectedCheckbox).parent().children("label").removeClass("loader-s");
        $("input[type=checkbox]").attr('disabled', false);//checkbox button disabling all. need to get the checkbox button in ths page
        $("input[type=checkbox]:checked").attr('disabled', false);
          this.utils.enableButton("updateProfileButton","UPDATE PROFILE");
      }
    },SystemError => {
      if(selectedCheckbox){
        $("#"+selectedCheckbox).parent().removeClass("load-check");
        $("input[type=checkbox]").attr('disabled', false);
        this.utils.enableButton("updateProfileButton","UPDATE PROFILE");
      }
    })
    .catch(error=>{});

  }

}
