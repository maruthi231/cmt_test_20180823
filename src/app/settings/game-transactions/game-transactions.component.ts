import { Component, OnInit, Input, SimpleChange, ViewEncapsulation, Output, EventEmitter, HostListener} from '@angular/core';

declare var $: any;
import 'intl-tel-input';
import 'bootstrap-select';

@Component({
  selector: 'app-game-transactions',
  templateUrl: './game-transactions.component.html',
  styleUrls: ['./game-transactions.component.scss'],
    encapsulation: ViewEncapsulation.None

})
export class GameTransactions implements OnInit {
  @Input() profileDetails;
  @Input() transactionDetails;
  @Input() currencyDetails;
  @Input() transactionRequestData;
  @Output() getTransactionDetails = new EventEmitter<any>();
  dateFormat = "yy-mm-dd";
  from;
  to;
  constructor() {}

  ngOnInit() {
  }

  @HostListener('window:resize') onResize() {
    // guard against resize before view is rendered
    var ww = document.body.clientWidth;
    if(ww <= 991){
      $( "#startDate-game" ).datepicker( "option", "numberOfMonths", 1 )
      $( "#endDate-game" ).datepicker( "option", "numberOfMonths", 1 )
    }else{
      $( "#startDate-game" ).datepicker( "option", "numberOfMonths", 2 )
      $( "#endDate-game" ).datepicker( "option", "numberOfMonths", 2 )
    }
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange
  }): void {
    changes['transactionDetails'] ? this.transactionDetails = changes['transactionDetails'].currentValue : '';
    changes['profileDetails'] ? this.profileDetails = changes['profileDetails'].currentValue : '';
    changes['currencyDetails'] ? this.currencyDetails = changes['currencyDetails'].currentValue : '';
    changes['transactionRequestData'] ? this.transactionRequestData = changes['transactionRequestData'].currentValue : '';
  }

  getDateToSet(date){
    date = new Date(date);
    return date.getFullYear()+"-"+(date.getMonth()+1 > 9 ? date.getMonth()+1 : '0'+(date.getMonth()+1))+"-"+date.getDate();
  }

  ngAfterViewInit(){

    var self = this;
    var ww = document.body.clientWidth;
    self.from = $( "#startDate-game" )
      .datepicker({
        defaultDate: "-1m",
        numberOfMonths: ww <= 991 ? 1 : 2,
        dateFormat: self.dateFormat
      })
      .on( "change", function() {
        self.to.datepicker( "option", "minDate", self.getDate( this ) );
        self.getTransactionDetailsOnChange("date");
      });
    self.to = $( "#endDate-game" ).datepicker({
      defaultDate: "-1m",
      numberOfMonths: ww <= 991 ? 1 : 2,
      dateFormat: self.dateFormat
    })
    .on( "change", function() {
      self.from.datepicker( "option", "maxDate", self.getDate( this ) );
      self.getTransactionDetailsOnChange("date");
    });
    this.setDateInDatePicker();
  }
  setDateInDatePicker(){
    if(this.transactionRequestData && this.transactionRequestData.interval && this.transactionRequestData.interval.start && this.transactionRequestData.interval.end && this.from && this.to){
      let fromDate = this.getDateToSet(this.transactionRequestData.interval.start);
      let toDate = this.getDateToSet(this.transactionRequestData.interval.end);
      this.from.datepicker( "setDate", fromDate  );
      this.to.datepicker( "setDate", toDate  );
    }
  }

  getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( this.dateFormat, element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }

  getTransactionDetailsOnChange(type){
    if(type == "date"){
      this.transactionRequestData.interval.start =   this.from.datepicker("getDate");
      this.transactionRequestData.interval.end =   this.to.datepicker("getDate");
      this.transactionRequestData.index = 0;
    }else if(type == "more" || type == "less" ){
      this.transactionRequestData.index = type == 'more' ? this.transactionRequestData.size + this.transactionRequestData.index  : (type == 'less' && this.transactionRequestData.index >0 && this.transactionRequestData.index - this.transactionRequestData.size > 0 ? this.transactionRequestData.index - this.transactionRequestData.size : 0);
    }
    this.getTransactionDetails.emit(this.transactionRequestData);
  }

}
