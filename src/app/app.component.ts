import { Component, Inject } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AjaxService } from './services/ajax.service';
import { UserDetailsService } from './services/user-details.service';
import { EmitterService } from './services/emitter.service';
import { Utility } from './utility/utility';
import { GameService } from './services/game.service';
import 'rxjs/add/operator/filter';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  routerEvent: any;
  countryCode;
  isCountryBlocked = false;
  countryBlockedEventSubs;
  isGameLaunch;
  isUserLoggedIn;
  resetPasswordToken;
  resetPassword;
  redirectTo;
  windowLocation;
  ssoEventSubs
  constructor(
    private router: Router,
    private ajaxService: AjaxService,
    private emitterService: EmitterService,
    private activatedRoute:ActivatedRoute,
    private utils:Utility,
    private gameService:GameService,
    private userDetailsService:UserDetailsService
  ) {
    this.countryBlockedEventSubs = this.emitterService.countryBlocked$.subscribe(
      country => {
        this.isCountryBlocked = true;
        this.countryCode = country;
      }
    )
    this.ssoEventSubs = this.emitterService.SSOError$.subscribe(
      ssoError => {
        if(ssoError){
            this.utils.closeAllOpenModel();
            this.emitterService.broadcastlogoutComplete("LogoutCompleted")
        }
      }
    )

    this.routerEvent = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((e) => {
      if($('link[rel=canonical]').length > 0){
        $('link[rel=canonical]').attr({'href':window.location.href});
      }else{
        $('head').append('<link rel="canonical" href="'+window.location.href+'" />')
      }
      this.windowLocation = window.location.pathname.substr(window.location.pathname.lastIndexOf("/")+1)

      this.activatedRoute.queryParamMap.subscribe(queryParams =>{
          if(queryParams.get('CXD')){
            this.utils.setAffIdTOCookie(queryParams.get('CXD'),"trackerIdCookie");
          }else if(queryParams.get('cxd')){
            this.utils.setAffIdTOCookie(queryParams.get('cxd'),"trackerIdCookie");
          }

          if(queryParams.get('Affid')){
            this.utils.setAffIdTOCookie(queryParams.get('Affid'),"affIdCookie");
          }else if(queryParams.get('affId')){
            this.utils.setAffIdTOCookie(queryParams.get('affId'),"affIdCookie");
          }else if(queryParams.get('AffId')){
            this.utils.setAffIdTOCookie(queryParams.get('AffId'),"affIdCookie");
          }else if(queryParams.get('affid')){
            this.utils.setAffIdTOCookie(queryParams.get('affid'),"affIdCookie");
          }

          if(queryParams.get('resetPassword')){
            this.resetPassword = queryParams.get('resetPassword');
          }
          if(queryParams.get('token')){
            this.resetPasswordToken = queryParams.get('token');
          }

          if(queryParams.get('redirect')){
            this.redirectTo = queryParams.get('redirect');
          }

          if(queryParams.get('forceGame')){
            this.gameService.isForcePlay = queryParams.get('forceGame');
          }

          window.history.replaceState(null, null, window.location.pathname);
      });
      window.scrollTo(0, 0);
      if (window.location.href.includes("gamePlay")) {
        this.isGameLaunch = true;
      } else {
        this.isGameLaunch = false;
      }
      if (this.countryCode == undefined || this.countryCode == "-NA-") {
        Promise.resolve(this.ajaxService.isBlockedCountry())
          .then(res => {
              if (res && res["is_IpBlocked"] && JSON.parse(res["is_IpBlocked"]) == true) {
                this.isCountryBlocked = true;
                this.countryCode = res["countryCode"];
              } else if (res && res["is_IpBlocked"] && JSON.parse(res["is_IpBlocked"]) == false) {
                this.isCountryBlocked = false;
                this.countryCode = res["countryCode"];
              } else {
                this.isCountryBlocked = false;
                this.countryCode = "-NA-";
              }
              this.userDetailsService.setUserBrowserCountry(this.countryCode);
            },
            SystemError => {}
          );
      } else {
        this.isCountryBlocked = false;
      }
    });
  }



  ngOnDestroy() {
    this.routerEvent.unsubscribe();
    this.countryBlockedEventSubs.unsubscribe();
  }


  // below function will add a .small-screen class to the html tag when screen reaches 991px
  ngOnInit() {
    this.utils.setSEO("","");
    this.isUserLoggedIn = this.utils.isUserLoggedIn();
    this.utils.loadPaymentIqEncryptor();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    var $window = $(window),
      $html = $('html');

    function resize() {
      if ($window.width() < 992) {
        return $html.addClass('small-screen');
      }

      $html.removeClass('small-screen');
    }

    $window
      .resize(resize)
      .trigger('resize');

  }

  ngAfterViewInit(){
    var self = this;
    $('body').on('click',function(e){
      self.utils.closeFixedFooter("");
      self.utils.closeLoginFields();
      $('.side-menu').removeClass("side-menu-opened");
      $('.push-side').removeClass("side-menu-opened");
      $('body').removeClass("backdrop-mask");
      $(".collapse.sub-menu").hide(200);
      $(".menu-icn i").removeClass("icon-minus").addClass("icon-plus");
      if(!$("#topUpButton").prop('disabled')){
        $('.quick-deposit-f-holder').addClass('hide');
        $('.quick-deposit-f-msg').addClass('hide');
        self.utils.hideAndShowQuickDeposit('deposit');
      }

    })

    $("#gameIframe body").on('click',function(e){
      console.log("iframe click");
    })


  }

}
