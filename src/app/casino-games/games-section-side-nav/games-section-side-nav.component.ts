import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { gameCategories } from '../config/game-config';
import { Utility } from '../../utility/utility';
import * as $ from 'jquery';

@Component({
  selector: 'app-games-section-side-nav',
  templateUrl: './games-section-side-nav.component.html',
  styleUrls: ['./games-section-side-nav.component.scss']
})
export class GamesSectionSideNavComponent implements OnInit {
  @Input() selectedCategory;
  @Output() filterCategory = new EventEmitter<string>();
  availableCategories;
  constructor(
    private utils:Utility
  ) {}

  ngOnInit() {
    this.availableCategories = gameCategories;
    var self = this;
    $(window).scroll(function() {
      self.utils.gameContentFixer();
    });
  }

  changeCategory(category){
    this.filterCategory.emit(category);
  }


}
