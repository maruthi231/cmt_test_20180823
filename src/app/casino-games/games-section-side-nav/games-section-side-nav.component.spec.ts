import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesSectionSideNavComponent } from './games-section-side-nav.component';

describe('GamesSectionSideNavComponent', () => {
  let component: GamesSectionSideNavComponent;
  let fixture: ComponentFixture<GamesSectionSideNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesSectionSideNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesSectionSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
