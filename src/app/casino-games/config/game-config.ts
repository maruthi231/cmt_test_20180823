export const gameCategories = [

  { category: "top20", displayName: 'Top 30' },
  { category: "videoslots", displayName: 'Video Slots' },
  { category: "newGames", displayName: 'New Games' },
  { category: "classicSlots", displayName: 'Classic Slots' },
  { category: "blackJack", displayName: 'Black Jack' },
  { category: "roulette", displayName: 'Roulette' },
  { category: "tableGames", displayName: 'Table Games' },
  { category: "videopoker", displayName: 'Video Poker' },
  { category: "jackpot", displayName: 'Jackpot' },
  { category: "other", displayName: 'Other Games' },
  { category: "allgames", displayName: 'All Games' },
  { category: "favourite", displayName: 'Favourite Games' }
];

//  { category: "3dslots", displayName: '3D Slots' },
