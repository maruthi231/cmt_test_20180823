import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesSectionContentComponent } from './games-section-content.component';

describe('GamesSectionContentComponent', () => {
  let component: GamesSectionContentComponent;
  let fixture: ComponentFixture<GamesSectionContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesSectionContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesSectionContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
