import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';
@Component({
  selector: 'app-form-radio',
  templateUrl: './form-radio.component.html',
  styleUrls: ['./form-radio.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormRadioComponent extends FormValidationComponent implements OnInit {
  config;
  group: FormGroup;
  selectedMethodData: any;
  callingFrom: any;
  usedAccount:boolean;
  constructor(
    private utils:Utility
  ) {
    super(utils);
  }

  ngOnInit() {
  }

}
