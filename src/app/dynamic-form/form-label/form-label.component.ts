import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Utility } from '../../utility/utility';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';
@Component({
  selector: 'app-form-label',
  templateUrl: './form-label.component.html',
  styleUrls: ['./form-label.component.scss']
})
export class FormLabelComponent extends FormValidationComponent implements OnInit {
  config;
  group: FormGroup;
  selectedMethodData: any;
  callingFrom: any;
  usedAccount:boolean;
  constructor(
    private utils:Utility
  ) {
    super(utils);
  }

  ngOnInit() {
  }
}
