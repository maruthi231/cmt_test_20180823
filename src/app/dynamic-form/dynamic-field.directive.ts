import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormButtonComponent } from './form-button/form-button.component';
import { FormInputComponent } from './form-input/form-input.component';
import { FormSelectComponent } from './form-select/form-select.component';
import { FormLabelComponent } from './form-label/form-label.component';
import { FormRadioComponent } from './form-radio/form-radio.component';

const components = {
  button: FormButtonComponent,
  tel: FormInputComponent,
  text: FormInputComponent,
  email: FormInputComponent,
  select: FormSelectComponent,
  label: FormLabelComponent,
  radio: FormRadioComponent
};

@Directive({
  selector: '[dynamicField]'
})
export class DynamicFieldDirective {
  @Input() config;
  @Input() group: FormGroup;
  @Input() selectedMethodData: any;
  @Input() callingFrom: any;
  @Output() buttonClicked = new EventEmitter<String>();
  @Input() usedAccount: boolean;
  component;
  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }

  ngOnInit(){
    const component = components[this.config.type];
    const factory = this.resolver.resolveComponentFactory<any>(component);
    this.component = this.container.createComponent(factory);
    this.component.instance.config = this.config;
    this.component.instance.group = this.group;
    this.component.instance.selectedMethodData = this.selectedMethodData;
    this.component.instance.callingFrom = this.callingFrom;
    this.component.instance.usedAccount = this.usedAccount;
    if(this.config.type == "button"){
      this.component.instance.buttonClicked = this.buttonClicked;
    }
  }

}
