import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginMobilePopupComponent } from './login-mobile-popup.component';

describe('LoginMobilePopupComponent', () => {
  let component: LoginMobilePopupComponent;
  let fixture: ComponentFixture<LoginMobilePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginMobilePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginMobilePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
