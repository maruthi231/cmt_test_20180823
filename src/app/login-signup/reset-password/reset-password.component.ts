import { Component, OnInit, Input,SimpleChange } from '@angular/core';
import { FormBuilder } from '@angular/forms'
import { CustomValidators } from '../../utility/custom-validator';
import { Utility } from '../../utility/utility';
import { AjaxService } from '../../services/ajax.service';
import { FormValidationComponent } from '../../shared-modules/form-validation/form-validation.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent extends FormValidationComponent implements OnInit {
  @Input() resetPasswordToken;
  serverError;
  newPasswordType = true;
  confirmPasswordType = true;
  focusedElement;
  resetPasswordForm = this.formBuilder.group({
    'password': [''],
    'confirmPassword': [''],
    'token': ['', [this.customValidators.required]]
  }, {validator: this.customValidators.passwordConfirming});
  constructor(
    private formBuilder:FormBuilder,
    private customValidators:CustomValidators,
    private utils:Utility,
    private ajaxService:AjaxService
  ) {
    super(utils);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: {
    [propName: string]: SimpleChange }): void {
    changes['resetPasswordToken'] && changes['resetPasswordToken'].currentValue ? this.resetPasswordToken = changes['resetPasswordToken'].currentValue : '';
    if (this.resetPasswordToken) {
      this.resetPasswordForm.controls["token"].setValue(this.resetPasswordToken);
    }
  }

  resetPassword(buttonId){
    this.serverError = ""
    if (this.resetPasswordForm.valid) {
      this.utils.disableButton(buttonId,'please wait...',true);
      let pwdDetails = this.utils.formControlToParams(this.resetPasswordForm,{});
      Promise.resolve(this.ajaxService.resetPassword(pwdDetails))
      .then(resetPasswordResp => {
        if(resetPasswordResp && resetPasswordResp["success"]){
          this.utils.enableButton(buttonId,"RESET PASSWORD");
          this.serverError = "RESET_PASSWORD_SUCCESS";
        }else{
          if(resetPasswordResp && resetPasswordResp["errors"]){
            this.serverError = resetPasswordResp["errors"][0].message;
          }else{
            this.serverError = "Something went wrong. Please try again later."
          }
        }
        this.utils.enableButton(buttonId,"RESET PASSWORD");
      },
      SystemError => {
        this.serverError = SystemError;
        this.utils.enableButton(buttonId,"RESET PASSWORD");
      });
    }else{
      this.utils.validateAllFormFields(this.resetPasswordForm);
    }
  }

  }
