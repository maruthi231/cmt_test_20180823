import { Component, OnInit } from '@angular/core';
import { EmitterService } from '../services/emitter.service';
import { Utility } from '../utility/utility';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor(
    private emitterService: EmitterService,
    private utils:Utility
  ) { }

  ngOnInit() {
    this.emitterService.broadcastHideheader("hide");
  }

  ngOnDestroy(){
    this.emitterService.broadcastHideheader("show")
  }

  gotToHome(){
    this.utils.closeModal("staticPagePopup",false,true);
  }

}
